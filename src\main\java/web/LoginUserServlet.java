package web;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import domain.User;
import javax.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginUserServlet extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");

            User user = User.authenticate(request.getParameter("username"), request.getParameter("password"));
            if (user == null) {
                PrintWriter out = response.getWriter();
                out.println("Either username or password is incorrect!");
                out.println("<a href=\"../\">Try again...</a>");
            }


            HttpSession session = request.getSession(true);
            session.setAttribute("username", user.getUsername());

            RequestDispatcher rd1 = request.getRequestDispatcher("welcome_page.jsp");
            rd1.forward(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
}

