package web.filters;

import domain.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by saviola on 3/20/16.
 */
@WebFilter({"/", "/register"})
public class RegistrationCosedFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException{

        if(User.getUser((String) request.getParameter("username"))!=null){
            request.setAttribute("registerError","User with this username already exists.");
            RequestDispatcher rd1 = request.getRequestDispatcher("register.jsp");
            rd1.forward(request,response);
            return;
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

}
