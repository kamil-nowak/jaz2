package database;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Db_Connection
{
    public Connection Connection()
    {
        try
        {

            String url = "jdbc:hsqldb:hsqldb://localhost/testdb";

            Connection myConnection = DriverManager.getConnection("jdbc:hsqldb:file:db/sjdb", "sa","");
            return myConnection;

        } catch (SQLException ex)
        {
            Logger.getLogger(Db_Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}