<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<center>
<table border="2" width="15%" cellpadding="5">
    <form action="register" method="post">
        <tr>
            <th colspan="2">Registration Form</th>
        </tr>
        <tr>
            <td>Username: </td>
            <td><input type="text" id="username" name="username" required/></td>
        </tr>
    <%
        if (request.getAttribute("registerError")!=null){
            out.print(request.getAttribute("registerError"));
        }

    %>
        <tr>
            <td>Password: </td>
            <td><input type="password" id="password" name="password" required/></td>
        </tr>
        <tr>
            <td>Confirm password: </td>
            <td><input type="password" id="password" name="confirmpassword" required/></td>
        </tr>
        <tr>
            <td>Email: </td>
            <td><input type="email" id="email" name="email" required/></td>
        </tr>
        <tr>
            <td>
                <a href="/">Login</a>
            </td>
            <td>
                <input type="submit" value="Register"/>
            </td>
        </tr>

    </form>

</table>
    </center>
</body>
</html>



